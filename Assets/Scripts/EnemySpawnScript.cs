﻿using UnityEngine;
using System.Collections;

public class EnemySpawnScript : MonoBehaviour {

    public Transform[] Enemies;
    public Transform[] EnemySpawnPoint;

    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, EnemySpawnPoint.Length);
        int EnemyIndex = Random.Range(0, EnemySpawnPoint.Length);
        Instantiate(Enemies[EnemyIndex], EnemySpawnPoint[spawnPointIndex].position, EnemySpawnPoint[spawnPointIndex].rotation);
    }

    // Use this for initialization
    void Start () {
        InvokeRepeating("Spawn", 4, 4);
            }
	
	// Update is called once per frame
	void Update ()
    {
        
    }
}
