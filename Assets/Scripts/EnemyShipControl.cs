﻿using UnityEngine;
using System.Collections;

public class EnemyShipControl : MonoBehaviour {

    public Transform EnemyPlasma;
    public Transform Enemy;
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate ()          
    {

        GameObject[] Tag = GameObject.FindGameObjectsWithTag("EnemyPlasma");
        int EnemyPlasmaCount = Tag.Length;

        if (EnemyPlasmaCount <= 0)
        {
        Instantiate(EnemyPlasma, Enemy.position, Quaternion.identity);
        }        
    }

}
