﻿using UnityEngine;
using System.Collections;

public class GUIScript : MonoBehaviour
{
    
    public GUISkin skin;
    private static int lives;
    private static int score;

    void OnGUI()
    {
        GUI.skin = skin;
        GUI.Label(new Rect(Screen.width-100,0,100,50), "Score: " + LivesScript.score);
        GUI.Label(new Rect(100, 0, 100, 50), "Lives: " + LivesScript.lives);

    }
}