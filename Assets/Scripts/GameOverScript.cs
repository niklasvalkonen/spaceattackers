﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {

    private static int lives;
    private static int score;

    void Start()
    {
        LivesScript.lives = 3;
        LivesScript.score = 0;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.LoadLevel("IntroScene");
        }

    }
}
