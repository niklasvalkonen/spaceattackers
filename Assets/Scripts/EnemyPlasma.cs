﻿using UnityEngine;
using System.Collections;

public class EnemyPlasma : MonoBehaviour
{

    private static int lives;

    public float speedFactor = 7.0f;
    public float timeToLiveSeconds = 8.0f;
    float aliveSeconds = 0.0f;

    
    public Transform player;
    public Transform playerspawnpoint;
 

    void Start()
    {
       
        rigidbody.velocity = Vector3.back * speedFactor;

    }

    void Update()
    {
        aliveSeconds += Time.deltaTime;
        if (aliveSeconds > timeToLiveSeconds)
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            Instantiate(player, playerspawnpoint.position, Quaternion.identity);
            LivesScript.lives--;
            Debug.Log(LivesScript.lives);
        }        
    }
}