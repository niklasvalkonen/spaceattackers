﻿using UnityEngine;
using System.Collections;

public class ShipController : MonoBehaviour
{

    public float speedFactor = 10.0f;
    public Transform PlayerPlasma;
    public Transform Player;


    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetAxis("Horizontal") < 0.0f)
        {
            rigidbody.AddForce(-transform.right * speedFactor);
        }
        else if (Input.GetAxis("Horizontal") > 0.0f)
        {
            rigidbody.AddForce(transform.right * speedFactor);
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Instantiate(PlayerPlasma, Player.position, Quaternion.identity);

        } 
    } 
}
