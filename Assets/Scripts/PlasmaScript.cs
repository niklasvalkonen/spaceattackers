﻿using UnityEngine;
using System.Collections;

public class PlasmaScript : MonoBehaviour {

    public float speedFactor = 7.0f;
    public float timeToLiveSeconds = 8.0f;
    float aliveSeconds = 0.0f;
    private static int score;

	void Start ()
    {
        rigidbody.velocity = Vector3.forward * speedFactor;

	}
		
	void Update ()
    {
        aliveSeconds += Time.deltaTime;
        if (aliveSeconds > timeToLiveSeconds)
        {
            Destroy(this.gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
            LivesScript.score = LivesScript.score+100;
        }
    }
}
